<?php
$ip = file("settings");

$SOCK = socket_create(AF_INET,SOCK_STREAM,SOL_TCP);
if (socket_connect($SOCK,$ip[0],$ip[1]) == FALSE)
{
	printf("BAD");
}
else
{
	if ($_GET["c"] == "getAlarm")
	{
		socket_write($SOCK,pack("a4LLLL","ALRM",202,0,4,$_GET["id"]));
		$header = unpack("a4MAGIC/LCOMMAND/LFLAGS/LSIZE",socket_read($SOCK,16));
		
		if($header["COMMAND"] == 203)
		{
			$alarmData = unpack("LEPOCH/LPERIOD/LIDENTIFIER/CSTATE/CTYPE/CACTION/CNAMELENGTH",socket_read($SOCK,16));
			$alarmName = socket_read($SOCK,$alarmData["NAMELENGTH"]);

			printf("<alarm><name>%s</name><id>%d</id><epoch>%d</epoch><period>%d</period><state>%d</state><type>%d</type><action>%d</action></alarm>",$alarmName,$alarmData["IDENTIFIER"],$alarmData["EPOCH"],$alarmData["PERIOD"],$alarmData["STATE"],$alarmData["TYPE"],$alarmData["ACTION"]);
		}
		else
		{
			printf("BADREPLY: %d",$header["COMMAND"]);
		}
	}
	else if ($_GET["c"] == "deleteAlarm")
	{
		socket_write($SOCK,pack("a4LLLL","ALRM",204,0,4,$_GET["id"]));
		$header = unpack("a4MAGIC/LCOMMAND/LFLAGS/LSIZE/LREPLY",socket_read($SOCK,20));
		printf("<reply>%d</reply>",$header["REPLY"]);
	}
	else if ($_GET["c"] == "listAlarms")
	{
		socket_write($SOCK,pack("a4LLL","ALRM",200,0,0));
		$header = unpack("a4MAGIC/LCOMMAND/LFLAGS/LSIZE",socket_read($SOCK,16));
		
		if($header["COMMAND"] == 201)
		{
			$left = $header["SIZE"];
			$count = 0;
			
			printf("<alarmlist>");
			
			while ($left > 0)
			{
				$alarmData = unpack("LEPOCH/LPERIOD/LIDENTIFIER/CSTATE/CTYPE/CACTION/CNAMELENGTH",socket_read($SOCK,16));
				$alarmName = socket_read($SOCK,$alarmData["NAMELENGTH"]);

				printf("<alarm><name>%s</name><id>%d</id><epoch>%d</epoch><period>%d</period><state>%d</state><type>%d</type><action>%d</action></alarm>",$alarmName,$alarmData["IDENTIFIER"],$alarmData["EPOCH"],$alarmData["PERIOD"],$alarmData["STATE"],$alarmData["TYPE"],$alarmData["ACTION"]);
				$left -= (16+$alarmData["NAMELENGTH"]);
				$count++;
			}
			
			printf("<count>%d</count></alarmlist>",$count);
		}
		else
		{
			printf("BADREPLY: %d",$header["COMMAND"]);
		}
	}
	else if ($_GET["c"] == "addAlarm")
	{
		socket_write($SOCK,pack("a4LLLLLLCCCCa".strlen($_GET["name"]),"ALRM",400,0,16+strlen($_GET["name"]),$_GET["epoch"],$_GET["period"],-1,$_GET["state"],$_GET["type"],$_GET["action"],strlen($_GET["name"]),$_GET["name"]));
		//socket_write($SOCK,pack("a4LLLLLLCCCCa".strlen($_GET["name"]),"ALRM",400,0,16+strlen($_GET["name"]),$_GET["epoch"],$_GET["period"],0,0,0,0,strlen($_GET["name"]),($_GET["name"])));
		$header = unpack("a4MAGIC/LCOMMAND/LFLAGS/LSIZE",socket_read($SOCK,16));
		
		if($header["COMMAND"] == 203)
		{
			$alarmData = unpack("LEPOCH/LPERIOD/LIDENTIFIER/CSTATE/CTYPE/CACTION/CNAMELENGTH",socket_read($SOCK,16));
			$alarmName = socket_read($SOCK,$alarmData["NAMELENGTH"]);

			printf("<alarm><namelen>%d</namelen><name>%s</name><id>%d</id><epoch>%d</epoch><period>%d</period><state>%d</state><type>%d</type><action>%d</action></alarm>",strlen($alarmName),$alarmName,$alarmData["IDENTIFIER"],$alarmData["EPOCH"],$alarmData["PERIOD"],$alarmData["STATE"],$alarmData["TYPE"],$alarmData["ACTION"]);
		}
		else
		{
			printf("BADREPLY: %d",$header["COMMAND"]);
		}
		
	}
	else if ($_GET["c"] == "editAlarm")
	{
		socket_write($SOCK,pack("a4LLLLLLCCCCa".strlen($_GET["name"]),"ALRM",400,0,16+strlen($_GET["name"]),$_GET["epoch"],$_GET["period"],$_GET["id"],$_GET["state"],$_GET["type"],$_GET["action"],strlen($_GET["name"]),$_GET["name"]));
		//socket_write($SOCK,pack("a4LLLLLLCCCCa".strlen($_GET["name"]),"ALRM",400,0,16+strlen($_GET["name"]),$_GET["epoch"],$_GET["period"],0,0,0,0,strlen($_GET["name"]),($_GET["name"])));
		$header = unpack("a4MAGIC/LCOMMAND/LFLAGS/LSIZE",socket_read($SOCK,16));
		
		if($header["COMMAND"] == 203)
		{
			$alarmData = unpack("LEPOCH/LPERIOD/LIDENTIFIER/CSTATE/CTYPE/CACTION/CNAMELENGTH",socket_read($SOCK,16));
			$alarmName = socket_read($SOCK,$alarmData["NAMELENGTH"]);

			printf("<alarm><namelen>%d</namelen><name>%s</name><id>%d</id><epoch>%d</epoch><period>%d</period><state>%d</state><type>%d</type><action>%d</action></alarm>",strlen($alarmName),$alarmName,$alarmData["IDENTIFIER"],$alarmData["EPOCH"],$alarmData["PERIOD"],$alarmData["STATE"],$alarmData["TYPE"],$alarmData["ACTION"]);
		}
		else
		{
			printf("BADREPLY: %d",$header["COMMAND"]);
		}
	}
	else if ($_GET["c"] == "listSounds")
	{
		socket_write($SOCK,pack("a4LLL","ALRM",300,0,0));
		$header = unpack("a4MAGIC/LCOMMAND/LFLAGS/LSIZE/LCOUNT",socket_read($SOCK,20));
		
		if($header["COMMAND"] == 301)
		{
			printf("<soundlist>");
			
			printf("<count>%d</count>",$header["COUNT"]);
			
			$i = 0;
			
			while($i < $header["COUNT"])
			{
				$len = unpack("CLEN",socket_read($SOCK,1));
				printf("<sound>%s</sound>",socket_read($SOCK,$len["LEN"]));
				$i++;
			}
			
			printf("</soundlist>");
		}
		else
		{
			printf("BADREPLY: %d",$header["COMMAND"]);
		}
	}
	else if ($_GET["c"] == "setSound")
	{
		socket_write($SOCK,pack("a4LLLLCa".strlen($_GET["sound"]),"ALRM",500,0,5+strlen($_GET["sound"]),$_GET["id"],strlen($_GET["sound"]),$_GET["sound"]));
	}
}

socket_close($SOCK);

?>
