<?php
	if ($_GET["u"] == 1)
	{
		$fd = fopen("settings","w");
		fwrite($fd,$_GET["ip"]."\n".$_GET["port"]);
		fclose($fd);
		
		$ip = $_GET["ip"];
		$port = $_GET["port"];
	}
	else
	{
		$ipdata = file("settings");
		$ip = $ipdata[0];
		$port = $ipdata[1];
	}
?>


<html lang="en">
<head>
  <meta charset="utf-8">
  <title>SuperAlarm Clock 2</title>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
  <script src="dep/jquery-1.11.1.min.js"></script>
  <script src="dep/jquery-ui-1.11.2/jquery-ui.min.js"></script>
  <link rel="stylesheet" href="styles.css">
  <script src="scripts.js"></script>
</head>
<body>

<div id="status-banner" class="ui-widget">
	<div class="ui-state-error ui-corner-all">
		<p>
			<span class="ui-icon ui-icon-alert"></span>
			<strong>Error:</strong> Failed to connect to backend
		</p>
	</div>
</div>

<div id="dialog-form" title="Alarm Setup">
	  <form>
		<fieldset>
		  <label for="name">Name</label>
		  <input type="text" name="name" id="name" value="Alarm" class="text ui-widget-content ui-corner-all">
		  <label for="date">Date</label>
		  <input type="text" name="date" id="date" value="13/12/2014" class="text ui-widget-content ui-corner-all">
		  <label for="time">Time</label>
		  <input type="text" name="time" id="time" value="12:00" class="text ui-widget-content ui-corner-all">
		  <label for="radioType">Type</label>
		  <div id="radioType">
			<input type="radio" name="radioType" id="typeMono" checked="checked"><label for="typeMono">Monotonic</label>
			<input type="radio" name="radioType" id="typeCont"><label for="typeCont" id="typeContL">Continuous</label>
		  </div>
		  <div id="op_period">
			<label for="period">Continuous Period (in seconds)</label>
			<input name="period" id="period" value="0" class="ui-corner-all">
		  </div>
		  <div id="req_action">
			  <label for="comboAction">Action</label>
			  <select name="comboAction" id="comboAction" class="combo">
				  <option value=0 selected="selected">Play WAV</option>
				  <option value=1>Notify (NYI)</option>
				  <option value=2>Send Email (NYI)</option>
				  <option value=3>External Program (NYI)</option>
			  </select>
		  </div>
		  <div id="op_sound">
			  <label for="comboSound">WAV To Play</label>
			  <select name="comboSound" id="comboSound" class="combo">
				  <option id="firstSound" selected="selected">Choose sound</option>
			</select>
		  </div>
		</fieldset>
	  </form>
	</div>

<div id="tabs" class="ui-widget">
	<ul>
		<li><a href="#alarm-contain">Alarms</a></li>
		<li><a href="#settings">Settings</a></li>
	</ul>

	<div id="alarm-contain" class="ui-widget">
	  <h1>Alarms:</h1>
	  <table id="alarms" class="ui-widget ui-widget-content">
		<thead>
		  <tr class="ui-widget-header ">
			<th>Name</th>
			<th>Date</th>
			<th>Action</th>
			<th>Edit</th>
		  </tr>
		</thead>
		<tbody>
		</tbody>
	  </table>
	  <button id="create-alarm">Add New Alarm</button>
	</div>
	<div id="settings">
		<form>
		<fieldset>
		  <label for="ip">IP Address</label>
		  <input type="text" name="ip" id="ip" value=<?php printf("%s",$ip); ?> class="text ui-widget-content ui-corner-all">
		  <label for="port">Port</label>
		  <input type="text" name="port" id="port" value=<?php printf("%d",$port); ?> class="text ui-widget-content ui-corner-all">
		  <input type="hidden" name="u" id="u" value="1" class="text ui-widget-content ui-corner-all">
		  <button id="update-ip">Update</button>
		 </fieldset>
		</form>
	</div>
</div>
 
</body>
</html>
