$(function() {
function deleteAlarm(event)
{
	dialog.dialog("close");
	dialog.drow = dialog.row;
	$.get("command.php?c=deleteAlarm&id="+dialog.row.find("#fname").find("div").text()).fail(onGeneralFailure).done(onDeleteSuccess);
}

function editAlarm(event)
{
	dialog.dialog("option","buttons", 
	{
		"Delete": deleteAlarm,
		"Save": updateAlarm,
		"Cancel":function()
		{
			dialog.dialog("close");
		}
	});
	$.get("command.php?c=getAlarm&id="+($(this).parent().parent().find("#fname").find("div").text())).done(onAlarmGetDone).fail(onGeneralFailure);
	dialog.dialog("open");
	dialog.row = $(this).parent().parent();
}

function updateAlarm(event)
{
	var type = 0;
	
	var formattedDate = $("#date").val().split("/");
	var formattedDate = (formattedDate[1]+"/"+formattedDate[0]+"/"+formattedDate[2]);
	
	var date = Date.parse((formattedDate+" "+$("#time").val()));
	
	if ($("#typeContL").attr("aria-pressed") == "true")
		type = 1;
	
	$.get("command.php?c=editAlarm&name="+$("#name").val()+"&epoch="+date/1000+"&period="+$("#period").val()+"&state=0&type="+type+"&action="+$("#comboAction").val()+"&id="+dialog.row.find("#fname").find("div").text()).done(onAlarmUpdateConfirm);
	dialog.dialog("close");
}

function onAlarmUpdateConfirm(data)
{
	if (data == "BAD")
		$("#status-banner").slideDown();
		
	xmlDoc = $.parseXML(data);	
	$xml = $(xmlDoc);
	
	var date = new Date(Number($xml.find("epoch").text())*1000);
	
	dialog.row.find("#fname").html($xml.find("name").text());
	dialog.row.find("#fdate").html(((date.getDate())+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+" "+(date.getHours())+":"+(date.getMinutes())));
	dialog.row.find("#faction").html(translateAction($xml.find("action").text()));
	
	$.get("command.php?c=setSound&id="+$xml.find("id").text()+"&sound="+$("#comboSound").val());
}

function onAlarmAddConfirm(data)
{
	if (data == "BAD")
		$("#status-banner").slideDown();
	
	xmlDoc = $.parseXML(data);	
	$xml = $(xmlDoc);
	
	var date = new Date(Number($xml.find("epoch").text())*1000);
	
	var ed = $('<button>Edit</button>').button({ icons: { primary: "ui-icon-gear" }, text: false }).on("click",editAlarm);
	var x = $("<tr>" +
		"<td id='fname'>" + $xml.find("name").text() + "<div class='hidden'>" + $xml.find("id").text() + "</div></td>" +
		"<td id='fdate'>" + ((date.getDate())+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+" "+(date.getHours())+":"+(date.getMinutes())) + "</td>" +
		"<td id='faction'>" + translateAction($xml.find("action").text()) + "</td>" +
		"<td>" + "<span class='edButtonPlaceholder'/>" + "</td>" +
		"</tr>");
	x.insertAfter("#alarms tbody");
	$("span.edButtonPlaceholder").replaceWith(ed);
	
	$.get("command.php?c=setSound&id="+$xml.find("id").text()+"&sound="+$("#comboSound").val());
}

function addAlarm() 
{
	dialog.dialog( "close" );
	
	var formattedDate = $("#date").val().split("/");
	var formattedDate = (formattedDate[1]+"/"+formattedDate[0]+"/"+formattedDate[2]);
	
	var date = Date.parse((formattedDate+" "+$("#time").val()));
	
	var type = 0;
	
	if ($("#typeContL").attr("aria-pressed") == "true")
		type = 1;
		
	$.get("command.php?c=addAlarm&name="+$("#name").val()+"&epoch="+date/1000+"&period="+$("#period").val()+"&state=0&type="+type+"&action="+$("#comboAction").val()).done(onAlarmAddConfirm);
  }

dialog = $("#dialog-form").dialog({
	autoOpen: false,
	height: 800,
	width: 350,
});

function onRadioClick(event)
{
	if ($("#typeContL").attr("aria-pressed") == "true")
	{
		$("#op_period").slideDown();
	}
	else
	{
		$("#op_period").slideUp();
	}
}

function onActionChange(event, ui)
{
	switch ($(this).val())
	{
		case "0":
			$("#op_sound").slideDown();
			break;
		case "1":
			$("#op_sound").slideUp();
			break;
		case "2":
			$("#op_sound").slideUp();
			break;
		case "3":
			$("#op_sound").slideUp();
			break;
	}
}

function onListAlarmDone(data)
{
	if (data == "BAD")
		$("#status-banner").slideDown();
	
	$("#field1").html(data);
	
	xmlDoc = $.parseXML(data);	
	$xml = $(xmlDoc);
	
	var count = $xml.find("count").text();
	
	$xml = $xml.find("alarm");
	
	$xml.each(function (index) 
	{	
		var date = new Date(Number($(this).find("epoch").text())*1000);
		
		var ed = $('<button>Edit</button>').button({ icons: { primary: "ui-icon-gear" }, text: false }).on("click",editAlarm);
		var x = $("<tr>" +
		"<td id='fname'>" + $(this).find("name").text() + "<div class='hidden'>" + $(this).find("id").text() + "</div></td>" +
		"<td id='fdate'>" + ((date.getDate())+"/"+(date.getMonth()+1)+"/"+date.getFullYear()+" "+(date.getHours())+":"+(date.getMinutes())) + "</td>" +
		"<td id='faction'>" + translateAction($(this).find("action").text()) + "</td>" +
		"<td>" + "<span class='edButtonPlaceholder'/>" + "</td>" +
		"</tr>");
		
		x.insertAfter("#alarms tbody");
		$("span.edButtonPlaceholder").replaceWith(ed);
	});
}

function onAlarmGetDone(data)
{
	if (data == "BAD")
		$("#status-banner").slideDown();
	
	xmlDoc = $.parseXML(data);	
	$xml = $(xmlDoc);
	
	$("#name").val($xml.find("name").text());
	$("#period").val($xml.find("period").text());
	
	var date = new Date(Number($xml.find("epoch").text())*1000);
	
	$("#date").val((date.getDate())+"/"+(date.getMonth()+1)+"/"+date.getFullYear());
	$("#time").val((date.getHours())+":"+(date.getMinutes()));
	$("#name").val($xml.find("name").text());
}

function onDeleteSuccess(data)
{
	xmlDoc = $.parseXML(data);	
	$xml = $(xmlDoc);
	
	if ($xml.find("reply").text() == 1)
		dialog.drow.remove();
	else
		alert("Failed to delete alarm");
}

function onGeneralFailure()
{
	$("#status-banner").slideDown();
}

$( "#create-alarm" ).button().on( "click", function() {
	dialog.dialog("option","buttons", 
	{
		"Create": addAlarm,
		"Cancel":function()
		{
			dialog.dialog("close");
		}
	});
	dialog.dialog( "open" );
});

function fillSounds(data)
{	
	if (data == "BAD")
		$("#status-banner").slideDown();
	
	xmlDoc = $.parseXML(data);	
	$$xml = $(xmlDoc);
	
	$snd = $$xml.find("soundlist").find("sound");

	for (i = 0; i < $$xml.find("count").text(); i++)
	{
		var html = $("<option>" + $snd.text() + "</option>");
		$snd = $snd.next();
		html.insertAfter("#firstSound");
	}
}

$("#date").datepicker();
$("#date").datepicker("option","dateFormat","dd/mm/yy");

$("#tabs").tabs();

$("#radioType").buttonset().on("click", onRadioClick);

$("#period").spinner();

$("#comboAction").selectmenu().on("selectmenuchange",onActionChange);

$("#comboSound").selectmenu();

$("#update-ip").button();

$.get("command.php?c=listAlarms").done(onListAlarmDone);

$.get("command.php?c=listSounds").done(fillSounds);

function translateAction(id)
{
	switch(id)
	{
		case "0":
			return "Play WAV";
		case "1":
			return "Notify (NYI)";
		case "2":
			return "Send Email (NYI)";
		case "3":
			return "External Program (NYI)";
		default:
			return "Unknown";
	}
}

});
